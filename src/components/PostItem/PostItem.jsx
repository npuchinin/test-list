import React from 'react';
import './style.scss';


// Элемент отображения поста
const PostItem = (props) => {
    const { icon_url, title, post_url } = props;
    return (
        <div onClick={() => {window.location.href = post_url}} className="postitem">
            <img alt={icon_url} className="postitem__icon" src={icon_url}/>
            <div className="postitem__title">
                {title}
            </div>
        </div>
    )
}
export default PostItem;