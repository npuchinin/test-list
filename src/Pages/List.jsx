import React, { useState, useEffect } from 'react';
import PostItem from '../components/PostItem/PostItem';
import './style.scss';
const axios = require("axios");
axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
const _ = require('lodash');

const List = () => {
    const [items, setItems] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [sortDirection, setSortDirection] = useState('asc');

    // Загрузка информации о постах
    useEffect(() => {
        axios.get("https://api.stackexchange.com/2.2/search?intitle=react&site=stackoverflow")
          .then(function (response) {
            const items = response?.data?.items
            const filteredResult = _.filter(items, function(el){ return el.is_answered === true });
            const sortedResult = _.sortBy(filteredResult, ['creation_date']);
            setItems(sortedResult);
            setIsLoading(false);
          })
          .catch(function (error) {
            console.log(error);
          })
    }, []);

    // Сортировака при смене направления сортировки
    useEffect(() => {
        const sortedResult = _.orderBy(items, ['creation_date'], [sortDirection]);
        setItems(sortedResult);
    //eslint-disable-next-line react-hooks/exhaustive-deps
    }, [sortDirection])

    // Смена напрявления сортировки
    const changeSortDirection = () => {
        if (sortDirection === 'asc') {
            setSortDirection('desc');
        } else {
            setSortDirection('asc');
        }
    }

    return (
        <div className="list">
            {
                isLoading ? 
                <div className="list__loading">
                    Loading...
                </div> :
                <div>
                    <button onClick={() => changeSortDirection()}>
                        Сортировать по дате
                    </button>
                    {items.map((el) => (
                        <PostItem
                            key={el.question_id}
                            icon_url={el?.owner?.profile_image}
                            post_url={el.link}
                            title={el.title}
                        />
                    ))}
                </div>
            }
        </div>
    )
}
export default List;