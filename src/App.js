import "./App.css";
import List from "./Pages/List";

function App() {
  // let items;
  // async function dataLoader() {
  //   let result;
  //   try {
  //     result = await axios.get(
  //       "https://api.stackexchange.com/2.2/search?intitle=react&site=stackoverflow"
  //     );
  //   } catch (e) {
  //     console.log(e);
  //   }
  //   items = result?.data?.items;
  // };
  // console.log(items);
  return (
    <div className="App">
      <List />
    </div>
  );
}

export default App;
